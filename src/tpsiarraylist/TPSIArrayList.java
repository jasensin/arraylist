/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tpsiarraylist;

import com.sun.org.apache.xalan.internal.xsltc.compiler.util.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

/**
 *
 * @author TPSI
 */
public class TPSIArrayList {

    static ferramentas tools = new ferramentas();

    static protected ArrayList<String> nomeilhas = new ArrayList<>();
    static protected ArrayList<Integer> ilhas = new ArrayList<>();
    static String con = null;
    static boolean continuar = true;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner sc = new Scanner(System.in);
        do {
            System.out.println("Introduza \n1- Ilhas \n2- Listar \n0- Sair");
            switch (sc.nextInt()) {
                case 1:
                    System.out.println("Perdente \n1- Adicionar \n2- Editar \n3- Eliminar");
                    boolean run = true;
                    int option = sc.nextInt();
                    switch (option) {
                        case 1:
                            do {
                                System.out.println("Numero para a nova ilha");
                                int numero = sc.nextInt();
                                if (!tools.ilhaExiste(numero)) {
                                    sc.nextLine();
                                    System.out.println("Nome da ilha");
                                    String nome = sc.nextLine();
                                    if (!tools.nomeExiste(nome)) {
                                        ilhas.add(numero);
                                        nomeilhas.add(nome);
                                        System.out.println("Continuar (s/n)");
                                        String opccao = sc.nextLine();
                                        switch (opccao) {
                                            case "s":
                                                run = true;
                                                break;
                                            case "n":
                                                run = false;
                                                break;
                                            default:
                                                System.err.println("Devera Introduzir (s/n)");
                                                run = true;
                                                break;
                                        }
                                    } else {
                                        System.err.println("Ja existe uma ilha com este nome");
                                    }
                                } else {
                                    System.err.println("Ja existe uma ilha com este numero");
                                }

                            } while (run);
                            System.out.println("Voltar ao menu principal (s/n)");
                            String opccao2 = sc.nextLine();
                            switch (opccao2) {
                                case "s":
                                    continuar = true;
                                    break;
                                case "n":
                                    continuar = false;
                                    break;
                                default:
                                    System.err.println("Devera Introduzir (s/n)");
                                    continuar = true;
                                    break;
                            }
                            break;
                        case 2:
                            System.out.println("Introduza o numero da ilha que quer alterar o nome");
                            int rename = sc.nextInt();
                            if (tools.ilhaExiste(rename)) {
                                sc.nextLine();
                                System.err.println("Novo nome");
                                String newnome = sc.nextLine();
                                if (!tools.nomeExiste(newnome)) {
                                    nomeilhas.set(rename - 1, newnome);
                                    System.out.println("Alterado com sucesso");
                                    run = false;
                                }else{
                                    System.err.println("JA existe uma ilha com este nome");
                                    run = true;
                                }

                            } else {
                                System.out.println("A ilha com o numero " + rename + " nao existe");
                                run = true;
                            }
                            break;

                        case 3:
                            do {
                                System.out.println("Introduza o numero da ilha");
                                int aEliminar = sc.nextInt();
                                if (tools.ilhaExiste(aEliminar)) {
                                    ilhas.remove(aEliminar);
                                    nomeilhas.remove(aEliminar);
                                    System.out.println("Eliminado com sucesso");
                                    run = false;
                                } else {
                                    System.out.println("A ilha com o numero " + aEliminar + " nao existe");
                                    run = true;
                                }
                            } while (run);
                            sc.nextLine();
                            System.out.println("Voltar ao menu principal (s/n)");
                            String opccao3 = sc.nextLine();
                            switch (opccao3) {
                                case "s":
                                    continuar = true;
                                    break;
                                case "n":
                                    continuar = false;
                                    break;
                                default:
                                    System.err.println("Devera Introduzir (s/n)");
                                    continuar = true;
                                    break;
                            }

                            break;
                        default:
                    }

                    break;
                case 2:

                    System.out.println("Perdente \n1- Listar por Numero \n2- Listar por Nome \n3- Listar tudo");
                    int option2 = sc.nextInt();
                    switch (option2) {
                        case 1:
                            System.out.println("Introduza o numero da ilha");
                            int numero = sc.nextInt();
                            tools.serchIlha(numero);
                            break;
                        case 2:
                            System.out.println("Introduza o nome da ilha");
                            sc.nextLine();
                            String nome = sc.nextLine();
                            tools.serchNome(nome);
                            break;
                        case 3:
                            System.out.println("Listagem de todas as Ilhas e seus numeros");
                            tools.serchAll();
                            break;
                        default:
                            break;
                    }

                    break;
                case 0:
                    continuar = false;
                    break;
                default:
                    System.err.println("Deve introduzir 1 a 3");
            }
        } while (continuar);

    }

    public static class ferramentas {

        private static void serchAll() {
            for (int i = 0; i < ilhas.size(); i++) {
                System.out.println("\t Ilha numero " + ilhas.get(i) + " chama-se " + nomeilhas.get(i));
            }
            System.out.println("Ilha nao existe");
        }

        private static void serchIlha(int ilha) {

            for (int i = 0; i < ilhas.size(); i++) {
                if (ilhas.get(i) == ilha) {
                    System.out.println("\t A ilha com o numero " + ilhas.get(i).toString() + " chama-se " + nomeilhas.get(i));
                }
            }
            System.out.println("Ilha nao existe");
        }

        private static void serchNome(String nome) {

            for (int i = 0; i < ilhas.size(); i++) {
                if (nomeilhas.get(i).toString().equals(nome)) {
                    System.out.println("\t A ilha com o nome " + nomeilhas.get(i) + " é a numero " + ilhas.get(i));
                }
            }
            System.out.println("Ilha nao existe");
        }

        private boolean ilhaExiste(int ilha) {
            for (int i = 0; i < ilhas.size(); i++) {
                if (ilhas.get(i).intValue() == ilha) {
                    return true;
                }

            }
            return false;
        }

        private boolean nomeExiste(String nome) {
            for (int i = 0; i < nomeilhas.size(); i++) {
                if (nomeilhas.get(i).toString().equals(nome)) {
                    return true;
                }
            }
            return false;
        }

    }

}
